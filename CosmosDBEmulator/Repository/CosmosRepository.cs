﻿using CosmosDBEmulator.Interface;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CosmosDBEmulator.Repository
{
    public abstract class CosmosRepository<T> : ICosmosRepositoryBase<T> where T : class
    {

        private DocumentClient client;
        private string Endpoint;
        private string Key;
        protected string DatabaseId;
        protected string CollectionId;

        public CosmosRepository(CosmosSettings settings)
        {
            Endpoint = settings.URI;
            Key = settings.PrimaryKey;
        }
        private async Task<bool> CollectionNotExists(Uri collectionUri)
        {
            try
            {
                await client.ReadDocumentCollectionAsync(collectionUri);
            }
            catch (DocumentClientException e)
            {
                return true;
            }

            return false;
        }


        public IQueryable<T> GetAllDocuments()
        {
            var collectionUri = UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId);
            if (CollectionNotExists(collectionUri).Result)
                return new List<T>().AsQueryable().OrderBy(x => x);


            return client.CreateDocumentQuery<T>(collectionUri);
        }

        public async Task<Document> CreateDocumentAsync(T Contact)
        {
            return await client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId), Contact);
        }

        public async Task<Document> UpdateDocumentAsync(string id, T Contact)
        {
            return await client.ReplaceDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id), Contact);
        }

        public async Task DeleteDocumentAsync(string id)
        {
            await client.DeleteDocumentAsync(UriFactory.CreateDocumentUri(DatabaseId, CollectionId, id));
        }

        public abstract void Setup();

        protected void Initialize()
        {
            client = new DocumentClient(new Uri(Endpoint), Key, new ConnectionPolicy { EnableEndpointDiscovery = false });
            CreateDatabaseIfNotExistsAsync().Wait();
            CreateCollectionIfNotExistsAsync().Wait();
        }

        protected virtual async Task CreateDatabaseIfNotExistsAsync()
        {
            try
            {
                await client.ReadDatabaseAsync(UriFactory.CreateDatabaseUri(DatabaseId));
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    await client.CreateDatabaseAsync(new Database { Id = DatabaseId });
                }
                else
                {
                    throw;
                }
            }
        }

        private async Task CreateCollectionIfNotExistsAsync()
        {
            try
            {
                await client.ReadDocumentCollectionAsync(UriFactory.CreateDocumentCollectionUri(DatabaseId, CollectionId));
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    await client.CreateDocumentCollectionAsync(
                        UriFactory.CreateDatabaseUri(DatabaseId),
                        new DocumentCollection { Id = CollectionId },
                        new RequestOptions { OfferThroughput = 1000 });
                }
                else
                {
                    throw;
                }
            }
        }
    }
}

﻿using CosmosDBEmulator.Interface;
using CosmosDBEmulator.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CosmosDBEmulator.Repository
{
    public class ContactRepository : CosmosRepository<Contact>, IContactRepository
    {

        public ContactRepository(CosmosSettings settings) : base(settings)
        {

        }

        public override void Setup()
        {
            CollectionId = Enum.GetName(typeof(CosmosCollections), CosmosCollections.Contacts);
            DatabaseId = Enum.GetName(typeof(CosmosDataBases), CosmosDataBases.CompanySearch);
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CosmosDBEmulator.Model
{
    public class Contact
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get;  set; }
        [JsonProperty(PropertyName = "email")]
        public string Email { get;set; }
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
         

    }
}

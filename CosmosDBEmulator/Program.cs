﻿using CosmosDBEmulator.Model;
using CosmosDBEmulator.Service;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace CosmosDBEmulator
{
    class Program
    {
        public static IConfigurationRoot Configuration { get; set; }

        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
               .AddEnvironmentVariables();

            IConfigurationRoot configuration = builder.Build();
            var mySettingsConfig = new CosmosSettings();
            configuration.GetSection("CosmosDBConfig").Bind(mySettingsConfig);

            var serviceProvider = new ServiceCollection()
            .AddSingleton<CosmosSettings>()
            .AddSingleton(mySettingsConfig)
            .AddSingleton<IContactService, ContactService>()
            .AddSingleton<ContactRepository>()
            .BuildServiceProvider();



            Console.WriteLine("Do you wanna insert new Contacts? Y/N");
            var key = Console.ReadLine();
            var contactService = serviceProvider.GetService<IContactService>();
            if (key.ToString().ToUpper() == "Y")
            {
                contactService.NewContact(new Model.Contact() { Name = "matheus", Email = "matheusmogi@gmail.com" });
                contactService.NewContact(new Model.Contact() { Name = "john", Email = "john@gmail.com" });
                Console.WriteLine("success");
            }
            var contacts = contactService.GetAllContacts();
            foreach (var contact in contacts)
            {
                Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(contact).ToString());
            }
            Console.WriteLine("Do you wanna update Contacts? Y/N");
            key = Console.ReadLine();
            if (key.ToString().ToUpper() == "Y")
            {
                foreach (var contact in contacts)
                {
                    contact.Name += " Updated";
                    contactService.EditContact(contact);
                }
               
            }
            foreach (var contact in contacts)
            {
                Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(contact).ToString());
            }
            Console.ReadKey();
        }


    }
}

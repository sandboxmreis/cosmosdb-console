﻿using CosmosDBEmulator.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CosmosDBEmulator.Interface
{
    public interface IContactRepository:ICosmosRepositoryBase<Contact>
    {
    }
}

﻿using Microsoft.Azure.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CosmosDBEmulator.Interface
{
    public interface ICosmosRepositoryBase<T> where T : class
    {
        IQueryable<T> GetAllDocuments();
        Task<Document> CreateDocumentAsync(T document);
        Task<Document> UpdateDocumentAsync(string id, T Contact);
        Task DeleteDocumentAsync(string id);

        void Setup();

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CosmosDBEmulator
{
    public class CosmosSettings
    {
        public string URI { get; set; }
        public string PrimaryKey { get; set; }
        public string DatabaseId { get; set; }
        public string CollectionId { get; set; }
    }
}

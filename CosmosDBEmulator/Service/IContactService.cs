﻿using CosmosDBEmulator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CosmosDBEmulator.Service
{
    public interface IContactService
    {
        void NewContact(Contact contact);
        IList<Contact> GetAllContacts();
        Contact GetContact(string id);
        void EditContact(Contact contact);
        void DeleteContact(string id);

    }
}

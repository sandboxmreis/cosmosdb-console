﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CosmosDBEmulator.Model;
using CosmosDBEmulator.Repository;

namespace CosmosDBEmulator.Service
{
    public class ContactService : IContactService
    {
        private readonly ContactRepository _repository;
        public ContactService(ContactRepository repository)
        {
            _repository = repository;
        }
        public void DeleteContact(string id)
        {
            throw new NotImplementedException();
        }

        public async void EditContact(Contact contact)
        {
           await _repository.UpdateDocumentAsync(contact.Id, contact);
        }

        public IList<Contact> GetAllContacts()
        {
            return  _repository.GetAllDocuments().ToList();
        }

        public Contact GetContact(string id)
        {
            throw new NotImplementedException();
        }

        public void NewContact(Contact contact)
        {
            try
            {
                _repository.CreateDocumentAsync(contact).Wait();

            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
